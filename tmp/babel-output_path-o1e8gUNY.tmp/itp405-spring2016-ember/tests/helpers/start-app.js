define('itp405-spring2016-ember/tests/helpers/start-app', ['exports', 'ember', 'itp405-spring2016-ember/app', 'itp405-spring2016-ember/config/environment'], function (exports, _ember, _itp405Spring2016EmberApp, _itp405Spring2016EmberConfigEnvironment) {
  exports['default'] = startApp;

  function startApp(attrs) {
    var application = undefined;

    var attributes = _ember['default'].merge({}, _itp405Spring2016EmberConfigEnvironment['default'].APP);
    attributes = _ember['default'].merge(attributes, attrs); // use defaults, but you can override;

    _ember['default'].run(function () {
      application = _itp405Spring2016EmberApp['default'].create(attributes);
      application.setupForTesting();
      application.injectTestHelpers();
    });

    return application;
  }
});