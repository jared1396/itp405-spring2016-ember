define('itp405-spring2016-ember/tests/helpers/resolver', ['exports', 'itp405-spring2016-ember/resolver', 'itp405-spring2016-ember/config/environment'], function (exports, _itp405Spring2016EmberResolver, _itp405Spring2016EmberConfigEnvironment) {

  var resolver = _itp405Spring2016EmberResolver['default'].create();

  resolver.namespace = {
    modulePrefix: _itp405Spring2016EmberConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _itp405Spring2016EmberConfigEnvironment['default'].podModulePrefix
  };

  exports['default'] = resolver;
});