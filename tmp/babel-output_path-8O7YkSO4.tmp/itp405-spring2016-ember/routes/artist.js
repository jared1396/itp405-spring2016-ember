define('itp405-spring2016-ember/routes/artist', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({
    model: function model(params) {
      //console.log(params);
      var id = params.id;
      //console.log(id);
      return $.getJSON('http://itp-api.herokuapp.com/artists/${id}');
    }

  });
});