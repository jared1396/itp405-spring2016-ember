define('itp405-spring2016-ember/components/app-version', ['exports', 'ember-cli-app-version/components/app-version', 'itp405-spring2016-ember/config/environment'], function (exports, _emberCliAppVersionComponentsAppVersion, _itp405Spring2016EmberConfigEnvironment) {

  var name = _itp405Spring2016EmberConfigEnvironment['default'].APP.name;
  var version = _itp405Spring2016EmberConfigEnvironment['default'].APP.version;

  exports['default'] = _emberCliAppVersionComponentsAppVersion['default'].extend({
    version: version,
    name: name
  });
});