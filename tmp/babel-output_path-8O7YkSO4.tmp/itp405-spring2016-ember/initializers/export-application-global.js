define('itp405-spring2016-ember/initializers/export-application-global', ['exports', 'ember', 'itp405-spring2016-ember/config/environment'], function (exports, _ember, _itp405Spring2016EmberConfigEnvironment) {
  exports.initialize = initialize;

  function initialize() {
    var application = arguments[1] || arguments[0];
    if (_itp405Spring2016EmberConfigEnvironment['default'].exportApplicationGlobal !== false) {
      var value = _itp405Spring2016EmberConfigEnvironment['default'].exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = _ember['default'].String.classify(_itp405Spring2016EmberConfigEnvironment['default'].modulePrefix);
      }

      if (!window[globalName]) {
        window[globalName] = application;

        application.reopen({
          willDestroy: function willDestroy() {
            this._super.apply(this, arguments);
            delete window[globalName];
          }
        });
      }
    }
  }

  exports['default'] = {
    name: 'export-application-global',

    initialize: initialize
  };
});