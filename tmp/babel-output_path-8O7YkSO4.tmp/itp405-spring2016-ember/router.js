define('itp405-spring2016-ember/router', ['exports', 'ember', 'itp405-spring2016-ember/config/environment'], function (exports, _ember, _itp405Spring2016EmberConfigEnvironment) {

  var Router = _ember['default'].Router.extend({
    location: _itp405Spring2016EmberConfigEnvironment['default'].locationType
  });

  Router.map(function () {
    this.route('artists');
    this.route('artists.artist', { path: 'artists/:id' });
  });

  exports['default'] = Router;
});