define('itp405-spring2016-ember/routes/artists', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({
    model: function model() {
      //console.log($.getJSON('http://itp-api.herokuapp.com/artists'));
      return $.getJSON('http://itp-api.herokuapp.com/artists');
    }

  });
});