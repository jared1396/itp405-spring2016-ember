define('itp405-spring2016-ember/routes/artist', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({
    model: function model(params) {

      var idOfArtist = params.id;
      return $.getJSON('http://itp-api.herokuapp.com/artists/${idOfArtist}');
    }

    // console log(1);

  });
});