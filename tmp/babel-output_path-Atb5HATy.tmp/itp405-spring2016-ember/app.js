define('itp405-spring2016-ember/app', ['exports', 'ember', 'itp405-spring2016-ember/resolver', 'ember-load-initializers', 'itp405-spring2016-ember/config/environment'], function (exports, _ember, _itp405Spring2016EmberResolver, _emberLoadInitializers, _itp405Spring2016EmberConfigEnvironment) {

  var App = undefined;

  _ember['default'].MODEL_FACTORY_INJECTIONS = true;

  App = _ember['default'].Application.extend({
    modulePrefix: _itp405Spring2016EmberConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _itp405Spring2016EmberConfigEnvironment['default'].podModulePrefix,
    Resolver: _itp405Spring2016EmberResolver['default']
  });

  (0, _emberLoadInitializers['default'])(App, _itp405Spring2016EmberConfigEnvironment['default'].modulePrefix);

  exports['default'] = App;
});