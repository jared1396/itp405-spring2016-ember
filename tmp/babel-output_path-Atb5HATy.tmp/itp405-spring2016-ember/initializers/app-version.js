define('itp405-spring2016-ember/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'itp405-spring2016-ember/config/environment'], function (exports, _emberCliAppVersionInitializerFactory, _itp405Spring2016EmberConfigEnvironment) {
  exports['default'] = {
    name: 'App Version',
    initialize: (0, _emberCliAppVersionInitializerFactory['default'])(_itp405Spring2016EmberConfigEnvironment['default'].APP.name, _itp405Spring2016EmberConfigEnvironment['default'].APP.version)
  };
});