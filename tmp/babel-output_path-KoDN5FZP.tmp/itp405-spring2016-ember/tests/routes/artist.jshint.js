define('itp405-spring2016-ember/tests/routes/artist.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - routes/artist.js');
  QUnit.test('should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(false, 'routes/artist.js should pass jshint.\nroutes/artist.js: line 6, col 10, \'id\' is defined but never used.\nroutes/artist.js: line 8, col 13, \'$\' is not defined.\n\n2 errors');
  });
});